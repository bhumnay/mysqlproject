package prwithmysql.demo.product_service.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import prwithmysql.demo.product_service.entity.Product;
import prwithmysql.demo.product_service.repo.ProductRepo;


@Service
public class KafkaListen {

    @Autowired
    private ProductRepo repo;

    @KafkaListener(topics = "product" , groupId = "product_group")
    public void consume(Product product){
        repo.save(product);
    }

}
