package prwithmysql.demo.product_service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import prwithmysql.demo.product_service.entity.Product;
import prwithmysql.demo.product_service.pojo.ProductDto;
import prwithmysql.demo.product_service.service.ProductService;
import prwithmysql.demo.product_service.service.ProductConverter;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
public class ProductController {

    @Autowired
    private ProductService service;
    @Autowired
    private ProductConverter converter;

    @PostMapping("/addProduct")
    public void addProduct(@RequestBody Product product){
        service.saveProduct(product);
    }

    @PostMapping("/addProducts")
    public List<ProductDto> addProduct(@RequestBody List<Product> products){
        return converter.entityToDto(service.saveProducts(products));
    }

    @GetMapping("/products")
    public List<ProductDto> findAllProducts(){
        return converter.entityToDto(service.getProducts());
    }

    @GetMapping("/productById/{id}")
    public ResponseEntity<ProductDto> findProductById(@PathVariable int id){
        try {
            ProductDto product = converter.entityToDto(service.getProductById(id));
            return new ResponseEntity<>(product, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/productByName/{name}")
    public ProductDto findProductByName(@PathVariable String name){
        return converter.entityToDto(service.getProductByName(name));
    }

    @PutMapping("/update")
    public ProductDto updateProduct(@RequestBody Product product){
        return converter.entityToDto(service.updateProduct(product));
    }

    @DeleteMapping("/delete/{id}")
    public String deleteProduct(@PathVariable int id){
        return service.deleteProduct(id);
    }
}
