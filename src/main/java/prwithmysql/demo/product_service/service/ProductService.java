package prwithmysql.demo.product_service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import prwithmysql.demo.product_service.entity.Product;
import prwithmysql.demo.product_service.repo.ProductRepo;

import java.util.List;

@Service
public class ProductService {
    @Autowired
    private ProductRepo repo;

    @Autowired
    private KafkaTemplate<String,Object> template;

    public void saveProduct(Product product) {
        template.send("product",product);
    }

    public List<Product> saveProducts(List<Product> products){
        return repo.saveAll(products);
    }

    public List<Product> getProducts(){
        return repo.findAll();
    }

    public Product getProductById(int id){
        return repo.findById(id).get();
    }

    public Product getProductByName(String name){
        return repo.findByName(name);
    }

    public String deleteProduct(int id){
        repo.deleteById(id);
        return "product removed"+id;
    }

    public Product updateProduct(Product product){
        Product existingProduct= repo.findById(product.getId()).orElse(null);
        existingProduct.setName(product.getName());
        existingProduct.setPrice(product.getPrice());
        existingProduct.setQuantity(product.getQuantity());
        return repo.save(existingProduct);
    }
}
