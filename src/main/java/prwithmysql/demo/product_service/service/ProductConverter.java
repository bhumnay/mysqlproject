package prwithmysql.demo.product_service.service;

import org.springframework.stereotype.Service;
import prwithmysql.demo.product_service.entity.Product;
import prwithmysql.demo.product_service.pojo.ProductDto;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductConverter {

    public ProductDto entityToDto(Product product){
        ProductDto dto = ProductDto.builder()
                .name(product.getName())
                .price(product.getPrice())
                .quantity(product.getQuantity())
                .build();
        return dto;
    }

    public List<ProductDto> entityToDto(List<Product> products){
        return products.stream().map(x-> entityToDto(x)).collect(Collectors.toList());
    }
}
